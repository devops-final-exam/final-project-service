package handlers

import (
	"assignment3/src/initializers"
	"assignment3/src/models"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func CreateOrderItem(c *gin.Context) {
	var item models.OrderItem

	if err := c.ShouldBindJSON(&item); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	var product models.Product

	initializers.DB.Where("id = ?", item.ProductID).Find(&product)
	item.TotalPrice = float64(item.Quantity) * product.Price

	id_user, err := c.Cookie("user_id")

	if err != nil {
		c.AbortWithError(http.StatusNotFound, err)
		return
	}

	var items []models.OrderItemInCart

	initializers.DB.Table("order_items i").
		Select("i.quantity as quantity, p.name as name, p.price as price, i.total_price as total_price, i.order_id as order_id, p.image as image, p.id as product_id, i.id as order_item_id").
		Joins("JOIN products p ON i.product_id = p.id").
		Joins("JOIN orders o ON i.order_id = o.id").
		Where("o.user_id = ? AND o.status = ? AND i.deleted_at IS NULL", id_user, "process").
		Find(&items)

	for _, val := range items {
		if val.ProductID == item.ProductID {
			c.AbortWithError(http.StatusBadRequest, fmt.Errorf("you already chose this product"))
			c.JSON(http.StatusBadRequest, gin.H{"message": "you already chose this product"})
			return
		}
	}

	if product.Quantity < item.Quantity {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("product quantity not enough"))
		c.JSON(http.StatusBadRequest, gin.H{"message": "product quantity not enough"})
		return
	} else {
		product.Quantity -= item.Quantity
		initializers.DB.Save(&product)
	}

	err = initializers.DB.Create(&item).Error

	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusCreated, item)

}

func GetAllOrderItems(ctx *gin.Context) {
	var items []models.OrderItem

	err := initializers.DB.Find(&items).Error
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	ctx.JSON(200, items)
}

func DeleteOrderItem(ctx *gin.Context) {
	id := ctx.Param("id")

	var item models.OrderItem

	initializers.DB.Where("id = ?", id).Find(&item)
	if item.ID == 0 {
		ctx.JSON(404, gin.H{"message": "Item not found!"})
		return
	}
	var product models.Product

	initializers.DB.Where("id = ?", item.ProductID).Find(&product)
	product.Quantity += item.Quantity
	initializers.DB.Save(&product)

	initializers.DB.Delete(&item)

	ctx.JSON(200, gin.H{"message": "Item deleted"})
}

func RetrieveOrderItem(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	var item models.OrderItem
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	initializers.DB.Where("id = ?", id).Find(&item)

	ctx.JSON(200, item)
}

func ItemOrderUser(ctx *gin.Context) {
	ID, err := ctx.Cookie("user_id")

	if err != nil {
		ctx.AbortWithError(200, err)
		return
	}

	var items []models.OrderItemInCart

	initializers.DB.Table("order_items i").
		Select("i.quantity as quantity, p.name as name, p.price as price, i.total_price as total_price, i.order_id as order_id, p.image as image, p.id as product_id, i.id as order_item_id").
		Joins("JOIN products p ON i.product_id = p.id").
		Joins("JOIN orders o ON i.order_id = o.id").
		Where("o.user_id = ? AND o.status = ? AND i.deleted_at IS NULL", ID, "process").
		Find(&items)

	var order models.Order
	if len(items) > 0 {
		orderIdCookie, err := ctx.Cookie("order_id")
		if err != nil {
			panic(err)
		}

		initializers.DB.Where("id = ?", orderIdCookie).Find(&order)
	}

	if len(items) == 0 {
		ctx.HTML(200, "cart.html", gin.H{"Items": items, "Empty": true, "TotalPrice": 0})
	} else {
		if order.ID == 0 {
			ctx.HTML(200, "cart.html", gin.H{"Items": items, "Empty": true, "TotalPrice": 0})
		} else {
			ctx.HTML(200, "cart.html", gin.H{"Items": items, "Empty": false, "TotalPrice": order.TotalPrice})
		}
	}
}

func DeleteAfterPurchase(ctx *gin.Context) {
	id := ctx.Param("id")

	var items []models.OrderItem

	initializers.DB.Where("order_id = ? ", id).Find(&items)

	if len(items) < 0 {
		ctx.JSON(404, gin.H{"message": "This order don't have items!"})
		return
	}

	initializers.DB.Delete(&items)

	ctx.JSON(200, gin.H{"message": "Items deleted!"})
}
