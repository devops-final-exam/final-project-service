package routes

import (
	"assignment3/src/handlers"

	"github.com/gin-gonic/gin"
)

func ProductRoutes(r *gin.Engine) {
	r.POST("/products/add", handlers.CreateProductHandler)
	r.GET("/products", handlers.GetAllProducts)
}

func AuthRoutes(r *gin.Engine) {
	r.GET("/register", handlers.ShowRegisterPage)
	r.POST("/register", handlers.RegisterHandler)
	r.GET("/login", handlers.ShowLoginPage)
	r.POST("/login", handlers.LoginHandler)
	r.POST("/logout", handlers.Logout)
}

func OrderRoutes(r *gin.Engine) {
	r.POST("/order", handlers.CreateOrder)
	r.GET("/order", handlers.GetAllOrders)
	r.GET("/order/:id", handlers.RetrieveOrder)
	r.PATCH("/order/:id", handlers.UpdateOrder)
	r.DELETE("/order/:id", handlers.DeleteOrder)
}

func OrderItemRoutes(r *gin.Engine) {
	r.GET("/order_items", handlers.GetAllOrderItems)
	r.POST("/order_items", handlers.CreateOrderItem)
	r.GET("/order_items/:id", handlers.RetrieveOrderItem)
	r.DELETE("/order_items/:id", handlers.DeleteOrderItem)
	r.DELETE("/order_items/:id/after_purchase", handlers.DeleteAfterPurchase)
}

func HtmlPageRoutes(r *gin.Engine) {
	r.LoadHTMLGlob("src/templates/*.html")
	r.GET("/", handlers.ShowMainPage)
	r.GET("/cart", handlers.ItemOrderUser)
	r.POST("/setcookie", handlers.SetNewOrderID)
}
