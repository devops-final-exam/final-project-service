package models

import (
	"errors"
	"gorm.io/gorm"
)

type Product struct {
	gorm.Model
	Name     string  `json:"name" gorm:"not null"`
	Price    float64 `json:"price" gorm:"not null"`
	Image    string  `json:"image"`
	Quantity int     `json:"quantity"`
	Items    []OrderItem
}

func (p *Product) Validate() error {

	if p.Name == "" {
		return errors.New("name required")
	}
	if p.Price <= 0 {
		return errors.New("price must be greater than zero")
	}
	return nil
}
